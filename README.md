# velero-sts

# mysql-pv backup

```bash
./velero install \
  --provider aws \
  --plugins velero/velero-plugin-for-aws:v1.9.0 \
  --bucket velero \
  --secret-file ./credentials-velero \
  --use-volume-snapshots=false \
  --backup-location-config region=minio,s3ForcePathStyle="true",s3Url=http://minio:9000

./velero backup create mysql-pv-backup --selector app=mysql --include-namespaces=mysql --default-volumes-to-fs-backup --snapshot-volumes=true
./velero backup describe mysql-pv-backup
./velero backup logs mysql-pv-backup
```

```bash
kubectl delete namespace/velero clusterrolebinding/velero
kubectl delete crds -l component=velero
```

# Another sources:

- https://blog.searce.com/kubernetes-backup-and-restore-with-velero-d7ad266ff7
- https://velero.io/docs/main/basic-install/