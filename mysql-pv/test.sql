-- Create the database
CREATE DATABASE IF NOT EXISTS your_database_name;

-- Switch to the newly created database
USE your_database_name;

-- Create a table
CREATE TABLE IF NOT EXISTS your_table_name (
    id INT AUTO_INCREMENT PRIMARY KEY,
    your_column_name VARCHAR(255)
);

-- Insert data into the table
INSERT INTO your_table_name (your_column_name) VALUES ('Row 1 data');
INSERT INTO your_table_name (your_column_name) VALUES ('Row 2 data');
INSERT INTO your_table_name (your_column_name) VALUES ('Row 3 data');
INSERT INTO your_table_name (your_column_name) VALUES ('Row 4 data');
INSERT INTO your_table_name (your_column_name) VALUES ('Row 5 data');

-- Display the inserted data
SELECT * FROM your_table_name;
